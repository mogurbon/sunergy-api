<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles= ['administrador', 'desarrollador', 'cliente'];

        foreach ($roles as $rol){

                $admin = new Role();
                $admin->type = $rol;
                $admin->save();

        }

    }
}
